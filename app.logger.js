(() => {
	'use strict'

	angular.module('prvstrapApp')
	.factory('logger', logger);

	logger.$inject = ["$log", "$mdToast"]

	function logger($log, $mdToast) {
		let service = {
			error  : error,
			// info   : info,
			// success: success,
			// warning: warning,
			log    : $log.log
		}
		return service

		function error(error, status) {
			$mdToast.show(
				$mdToast.simple()
				.textContent(error)
				.position('top right')
				.hideDelay(3000)
				)
			$log.error(`Error: ${error} ${status}`)
		}
	}
})()