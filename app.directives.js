(() => {
  'use strict'

  angular.module('aditjobsApp')
  .directive('updateTitle', updateTitle)

  updateTitle.$inject = ['$rootScope', '$timeout']
  function updateTitle($rootScope, $timeout) {
    return {
      link: function(scope, element) {

        var listener = function(event, toState) {

          var title = 'Connecting Job Seekers and Employers';
          if (toState.data && toState.data.pageTitle) title = toState.data.pageTitle;

          $timeout(function() {
            element.text(title);
          }, 0, false);
        };

        $rootScope.$on('$stateChangeSuccess', listener);
      }
    };
  }
})()

