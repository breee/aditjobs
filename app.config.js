(() => {
  'use strict';
  angular
  .module('aditjobsApp', ['ngTouch', 'ngAnimate', 'ui.router', 'ui.router.stateHelper', 'angularSpinners', 'angular-loading-bar', 'angular-carousel', 'zumba.angular-waypoints', 'smoothScroll', 'angularjs-dropdown-multiselect', 'angularUtils.directives.dirPagination', 'ng-file-model', 'ngInputModified', 'angular-toArrayFilter', 'esc-key'])
  .constant('CONFIG', 
  {
    api_base_url: 'http://192.168.1.51/aditjobs/api',
    api_base_url_dev: 'http://localhost:3000'
  })
  .config(($httpProvider) => {
    $httpProvider.interceptors.push('TokenInterceptor')
  })
  .run(function($rootScope, $state, $window, AuthenticationService) {
    $rootScope.$on("$stateChangeStart", function(event, nextRoute, currentRoute) {
        //redirect only if both isAuthenticated is false and no token is set
        if (nextRoute != null && nextRoute.access != null && nextRoute.access.requiredAuthentication
          && !AuthenticationService.isAuthenticated && !$window.sessionStorage.token) {
          event.preventDefault()
        $state.transitionTo('login')  
      }
    $rootScope.$state = $state
    })
  })
})()
