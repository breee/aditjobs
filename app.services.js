(() => {
	'use strict'

	angular
	.module('aditjobsApp')
	.factory('AuthenticationService', AuthenticationService)	
  .factory('TokenInterceptor', TokenInterceptor)  

  AuthenticationService.$inject = []
  TokenInterceptor.$inject      = ['$q', '$window', '$location', '$injector', 'AuthenticationService']

  function AuthenticationService() {
    let auth = {
     isAuthenticated: false,
     isAdmin        : false,
     isManager      : false
   }

   return auth
 }

 function TokenInterceptor($q, $window, $location, $injector, AuthenticationService){
  return {
   request: function (config) {
        //      config.headers = config.headers || {};
        if ($window.sessionStorage.token) {
        	config.headers['x-jwt-token'] = $window.sessionStorage.token;
        }
        return config
      },

      requestError: function(rejection) {
      	return $q.reject(rejection);
      },

      /* Set Authentication.isAuthenticated to true if 200 received */
      response: function (response) {
      	if (response != null && response.status == 200 && $window.sessionStorage.token && !AuthenticationService.isAuthenticated) {
      		AuthenticationService.isAuthenticated = true;
      	}
      	return response || $q.when(response);
      },

      /* Revoke client authentication if 401 is received */
      responseError: function(rejection) {
      	if (rejection != null && rejection.status === 401 && ($window.sessionStorage.token || AuthenticationService.isAuthenticated)) {
      		delete $window.sessionStorage.token;
      		AuthenticationService.isAuthenticated = false;
      		$injector.get('$state').transitionTo('login')
      	}

      	return $q.reject(rejection);
      }
    }
  }

})()