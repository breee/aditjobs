(() => {
	'use strict'

	angular.module('aditjobsApp')
	.config(config)

	config.$inject = ["$stateProvider", "$urlRouterProvider"]

	function config($stateProvider, $urlRouterProvider) {
		$stateProvider
		.state('login', {
			url: '/login',
			templateUrl: 'components/login/views/login.view.html',
			controller: 'loginController',
			controllerAs: 'vm',
			resolve: {
				loginService: loginService
			}
		})
		$urlRouterProvider.otherwise('/404')
	}

	loginService.$inject = ['apiLoginService'];
	function loginService(apiLoginService) {
		return apiLoginService.signIn
	}
})()