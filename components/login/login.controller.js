(() => {
  'use strict'

  angular.module('aditjobsApp')
  .controller('loginController', loginController);

  loginController.$inject = ["$http","$window","$state","AuthenticationService","CONFIG","loginService"]

  function loginController($http, $window, $state, AuthenticationService, CONFIG, loginService) {

    var vm = this
    vm.login = (username, password) => {
      console.log({username, password})
      if (username != null && password != null) {

        loginService(username, password)
        .then(function (data) {
          AuthenticationService.isAuthenticated = true
          $window.sessionStorage.token = data.token
          $state.go('main.pricing')
        }, function (error) {
          console.log(error)
        }).finally(function(){ 
          console.log('test')
        })
      }
    }
  }
})()