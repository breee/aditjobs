(() => {
  'use strict'

  angular
  .module('aditjobsApp')
  .factory('apiLoginService', apiLoginService)

  apiLoginService.$inject = ['$http', '$q', 'CONFIG']

  function apiLoginService($http, $q, CONFIG) {


    return {
      signIn : signIn,
      signOut: signOut
    }

    function signIn(username, password) {
      let deffered = $q.defer()
      $http({
        method: 'POST',
        url   : CONFIG.api_base_url + '/login',
        data  : { username: username, password: password},
        headers : { 'Content-Type' : 'application/json'},
        cache : false
      })
      .then(fetchTokenComplete) 
      .catch(fetchTokenFailed)
      return deffered.promise

      function fetchTokenComplete(response) {
        if (response != undefined && typeof response == "object") {
          deffered.resolve(response.data)
        } else {
          logger.error('Invalid Server request')
        }
      }

      function fetchTokenFailed(error) {
        deffered.reject(error)
      }
    }

    function signOut() {
      let deffered = $q.defer()
      $http({
        method: 'GET',
        url   : CONFIG.api_base_url + '/logout',
        cache : false
      })
      .then(logoutComplete) 
      .catch(logoutFailed)
      return deffered.promise

      function logoutComplete(response) {
        deffered.resolve(response)
      }

      function logoutFailed(error) {
        deffered.reject(error)
      }
    }
  }
})()