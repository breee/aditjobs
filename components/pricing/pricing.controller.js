(() => {
  'use strict'

  angular.module('aditjobsApp')
  .controller('pricingController', pricingController);

  pricingController.$inject = ["$q", "$window", "$state", "AuthenticationService"]

  function pricingController($q, $window, $state, AuthenticationService) {

    var vm = this

    vm.isAuthenticated = AuthenticationService.isAuthenticated

    vm.test = () => {
      vm.test = 'test inner page'
      angular.element(document.querySelector('.signup-panel')).css('background-color', 'red')
    }
    
    activate()

    function activate() {
      var promises = []
      
      return $q.all(promises).then(function() {
        console.log('Activated Pricing View')
      })
    }

    
  }
})()