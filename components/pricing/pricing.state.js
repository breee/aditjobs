(() => {
  'use strict'

  angular.module('aditjobsApp')
  .config(config)

  config.$inject = ["stateHelperProvider", "$urlRouterProvider"]

  function config(stateHelperProvider, $urlRouterProvider) {
    stateHelperProvider
    .setNestedState({
      name        : 'main.pricing',
      url         : '/pricing',
      data        : { 
        pageTitle: 'Check our offers - Aditjobs',
        mastheadClass: 'pricing'
      },
      views       : {
        '': {
          templateUrl : 'components/pricing/views/pricing.view.html',
          controller  : "pricingController",
          controllerAs: 'vm',
          // resolve: {
          //   jobFilters: jobFilters,
          //   jobPosts  : jobPosts,
          // }
        },
        // 'testimonial@main.home': { 
        //   templateUrl : 'components/home/views/testimonial.view.html',
        // }
      }
    })
    
    $urlRouterProvider.otherwise('/404')
  }

// jobFilters.$injector = ['browseJobsService']
//   function jobFilters (browseJobsService) {
//     return browseJobsService.jobFilters
// }

// jobPosts.$injector = ['browseJobsService']
//   function jobPosts (browseJobsService) {
//     return browseJobsService.jobPosts
// }

})()

