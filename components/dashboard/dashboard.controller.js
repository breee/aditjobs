(() => {
  'use strict'

  angular.module('prvstrapApp')
  .controller('dashboardController', dashboardController);

  dashboardController.$inject = ['$mdSidenav', '$mdUtil', '$timeout', '$state', '$http', '$window', 'AuthenticationService', 'apiLoginService', '$injector', 'spinnerService']

  function dashboardController($mdSidenav, $mdUtil, $timeout, $state, $http, $window, AuthenticationService, apiLoginService, $injector, spinnerService) {

    var vm = this
    vm.viewStateTitle = $state.current.data.pageTitle

  }
})()