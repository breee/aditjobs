(() => {
  'use strict'

  angular
  .module('prvstrapApp')
  .factory('dashboardHomeService', dashboardHomeService)

  dashboardHomeService.$inject = ['$http', '$q', 'logger', 'CONFIG']

  function dashboardHomeService($http, $q, logger, CONFIG) {
    let platform_type = ['im','sms','email','mode']

    return {
      lineGraph              : lineGraph,
      imTotalStats           : imTotalStats,
      smsTotalStats          : smsTotalStats,
      emailTotalStats        : emailTotalStats,
      modeTotalStats         : modeTotalStats,
      freelancerActivityStats: freelancerActivityStats,
      fulltimerActivityStats : fulltimerActivityStats,
      topFreelancerStats     : topFreelancerStats,
      topFulltimerStats      : topFulltimerStats,
      topTeamStats           : topTeamStats,
      platformStats          : platformStats,
      platformByService      : platformByService
    }

    function lineGraph() {
      let deffered = $q.defer()

      $http({
        method : 'GET',
        url    : `${CONFIG.api_base_url}/dashboard/linegraph`,
        headers: { 'Content-Type' : 'application/json'}
      })
      .then(fetchLineGraphComplete) 
      .catch(fetchLineGraphFailed)
      return deffered.promise

      function fetchLineGraphComplete(response) {
        if (response != undefined && typeof response == "object") {
          deffered.resolve(response.data)
        } else {
          logger.error('Invalid Server request')
        }
      }

      function fetchLineGraphFailed(error) {
        deffered.reject(error)
      }
    }

    function platformByService() {
      let promises = []
      platform_type.map( type => {
        let deffered = $q.defer()
        $http({ 
          method : 'GET',
          url    : `${CONFIG.api_base_url}/dashboard/${type}-total-stats`,
          headers: { 'Content-Type' : 'application/json'}
        }).then(function(response){
          deffered.resolve(response.data)
        }).catch(function(error){
          deffered.reject(error)
        })
        promises.push(deffered.promise)
      })
      return $q.all(promises)
    }

    function imTotalStats() {
      let deffered = $q.defer()

      $http({
        method : 'GET',
        url    : `${CONFIG.api_base_url}/dashboard/im-total-stats`,
        headers: { 'Content-Type' : 'application/json'}
      })
      .then(fetchImTotalStatsComplete) 
      .catch(fetchImTotalStatsFailed)
      return deffered.promise

      function fetchImTotalStatsComplete(response) {
        if (response != undefined && typeof response == "object") {
          deffered.resolve(response.data)
        } else {
          logger.error('Invalid Server request')
        }
      }

      function fetchImTotalStatsFailed(error) {
        deffered.reject(error)
      }
    }

    function smsTotalStats() {
      let deffered = $q.defer()

      $http({
        method : 'GET',
        url    : `${CONFIG.api_base_url}/dashboard/sms-total-stats`,
        headers: { 'Content-Type' : 'application/json'}
      })
      .then(fetchSmsTotalStatsComplete) 
      .catch(fetchSmsTotalStatsFailed)
      return deffered.promise

      function fetchSmsTotalStatsComplete(response) {
        if (response != undefined && typeof response == "object") {
          deffered.resolve(response.data)
        } else {
          logger.error('Invalid Server request')
        }
      }

      function fetchSmsTotalStatsFailed(error) {
        deffered.reject(error)
      }
    }

    function emailTotalStats() {
      let deffered = $q.defer()

      $http({
        method : 'GET',
        url    : `${CONFIG.api_base_url}/dashboard/email-total-stats`,
        headers: { 'Content-Type' : 'application/json'}
      })
      .then(fetchEmailTotalStatsComplete) 
      .catch(fetchEmailTotalStatsFailed)
      return deffered.promise

      function fetchEmailTotalStatsComplete(response) {
        if (response != undefined && typeof response == "object") {
          deffered.resolve(response.data)
        } else {
          logger.error('Invalid Server request')
        }
      }

      function fetchEmailTotalStatsFailed(error) {
        deffered.reject(error)
      }
    }

    function modeTotalStats() {
      let deffered = $q.defer()

      $http({
        method : 'GET',
        url    : `${CONFIG.api_base_url}/dashboard/mode-total-stats`,
        headers: { 'Content-Type' : 'application/json'}
      })
      .then(fetchModeTotalStatsComplete) 
      .catch(fetchModeTotalStatsFailed)
      return deffered.promise

      function fetchModeTotalStatsComplete(response) {
        if (response != undefined && typeof response == "object") {
          deffered.resolve(response.data)
        } else {
          logger.error('Invalid Server request')
        }
      }

      function fetchModeTotalStatsFailed(error) {
        deffered.reject(error)
      }
    }

    function freelancerActivityStats() {
      let deffered = $q.defer()

      $http({
        method : 'GET',
        url    : `${CONFIG.api_base_url}/dashboard/freelancer-activity-stats`,
        headers: { 'Content-Type' : 'application/json'}
      })
      .then(fetchFreelancerActivityStatsComplete) 
      .catch(fetchFreelancerActivityStatsFailed)
      return deffered.promise

      function fetchFreelancerActivityStatsComplete(response) {
        if (response != undefined && typeof response == "object") {
          deffered.resolve(response.data)
        } else {
          logger.error('Invalid Server request')
        }
      }

      function fetchFreelancerActivityStatsFailed(error) {
        deffered.reject(error)
      }
    }

    function fulltimerActivityStats() {
      let deffered = $q.defer()

      $http({
        method : 'GET',
        url    : `${CONFIG.api_base_url}/dashboard/fulltimer-activity-stats`,
        headers: { 'Content-Type' : 'application/json'}
      })
      .then(fetchFulltimerActivityStatsComplete) 
      .catch(fetchFulltimerActivityStatsFailed)
      return deffered.promise

      function fetchFulltimerActivityStatsComplete(response) {
        if (response != undefined && typeof response == "object") {
          deffered.resolve(response.data)
        } else {
          logger.error('Invalid Server request')
        }
      }

      function fetchFulltimerActivityStatsFailed(error) {
        deffered.reject(error)
      }
    }

    function topFreelancerStats() {
      let deffered = $q.defer()

      $http({
        method : 'GET',
        url    : `${CONFIG.api_base_url}/dashboard/top-freelancer-stats`,
        headers: { 'Content-Type' : 'application/json'}
      })
      .then(fetchtopFreelancerStatsComplete) 
      .catch(fetchtopFreelancerStatsFailed)
      return deffered.promise

      function fetchtopFreelancerStatsComplete(response) {
        if (response != undefined && typeof response == "object") {
          deffered.resolve(response.data)
        } else {
          logger.error('Invalid Server request')
        }
      }

      function fetchtopFreelancerStatsFailed(error) {
        deffered.reject(error)
      }
    }

    function topFulltimerStats() {
      let deffered = $q.defer()

      $http({
        method : 'GET',
        url    : `${CONFIG.api_base_url}/dashboard/top-fulltimer-stats`,
        headers: { 'Content-Type' : 'application/json'}
      })
      .then(fetchtopFulltimerStatsComplete) 
      .catch(fetchtopFulltimerStatsFailed)
      return deffered.promise

      function fetchtopFulltimerStatsComplete(response) {
        if (response != undefined && typeof response == "object") {
          deffered.resolve(response.data)
        } else {
          logger.error('Invalid Server request')
        }
      }

      function fetchtopFulltimerStatsFailed(error) {
        deffered.reject(error)
      }
    }

    function topTeamStats() {
      let deffered = $q.defer()

      $http({
        method : 'GET',
        url    : `${CONFIG.api_base_url}/dashboard/top-team-stats`,
        headers: { 'Content-Type' : 'application/json'}
      })
      .then(fetchtopTeamStatsComplete) 
      .catch(fetchtopTeamStatsFailed)
      return deffered.promise

      function fetchtopTeamStatsComplete(response) {
        if (response != undefined && typeof response == "object") {
          deffered.resolve(response.data)
        } else {
          logger.error('Invalid Server request')
        }
      }

      function fetchtopTeamStatsFailed(error) {
        deffered.reject(error)
      }
    }

    function platformStats() {
      let deffered = $q.defer()

      $http({
        method : 'GET',
        url    : `${CONFIG.api_base_url}/dashboard/platform-stats`,
        headers: { 'Content-Type' : 'application/json'}
      })
      .then(fetchplatformStatsComplete) 
      .catch(fetchplatformStatsFailed)
      return deffered.promise

      function fetchplatformStatsComplete(response) {
        if (response != undefined && typeof response == "object") {
          deffered.resolve(response.data)
        } else {
          logger.error('Invalid Server request')
        }
      }

      function fetchplatformStatsFailed(error) {
        deffered.reject(error)
      }
    }

  }
})()