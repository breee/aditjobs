(() => {
  'use strict'

  angular.module('prvstrapApp')
  .controller('dashboardHomeController', dashboardHomeController);

  dashboardHomeController.$inject = [
  '$timeout', '$q', '$state', '$http', 'AuthenticationService','$injector', 'spinnerService',
  'lineGraph', 'imTotalStats','smsTotalStats', 'emailTotalStats', 'modeTotalStats',
  'freelancerStats', 'fulltimerStats', 'topFreelancerStats', 'topFulltimerStats',
  'platformStats', 'topTeamStats', 'logger', 'platformByService']

  function dashboardHomeController($timeout, $q, $state, $http, AuthenticationService, $injector,
    spinnerService, lineGraph, imTotalStats, smsTotalStats, emailTotalStats, modeTotalStats, 
    freelancerStats, fulltimerStats, topFreelancerStats, topFulltimerStats,
    platformStats, topTeamStats, logger, platformByService) {

    var vm = this
    
    vm.SubTitle = $state.current.data.subTitle
    vm.optionsDn = {
      animate:{
        duration: 2000,
        enabled: false
      },
      lineWidth: 10,
      size: 130,
      scaleColor: false,
      trackColor: 'rgba(255,255,255,.25)',
      barColor: '#4CAF50'
    }

    vm.optionsAvgStats = {
      animate:{
        duration: 4000,
        enabled: false
      },
      lineWidth: 20,
      size: 220,
      scaleColor: false,
      trackColor: 'rgba(255,255,255,.25)',
      barColor: '#4CAF50'
    }

    vm.linegraphDataset     = []
    vm.statsLabel           = ["S", "M", "T", "W", "T", "F", "S"]
    vm.statsSeries          = ['Last Week', 'This Week']
    vm.statsOptions         = { legend: { display: true, labels: { fontColor: '#fff'}} }
    vm.activityLabel        = ["Active", "Idle", "Total"]
    vm.freelancerPieDataset = {}
    vm.fulltimerPieDataset  = {}
    vm.topFreelancerStats   = []
    vm.topFreelancerStats   = []
    vm.topFulltimerStats    = []
    vm.topTeamStats         = []
    vm.platformStats        = []
    vm.platformByService    = []

    activate()

    function activate() {
      var promises = [lgraphAvgStats(), freelancerActivityStats(), fulltimerActivityStats(),
      freelancerTopStats(), fulltimerTopStats(), teamTopStats(), platformTotalStats(), returnQall()]
      
      return $q.all(promises).then(function() {
        console.log('Activated Dashboard View')
      })
    }

    function returnQall() {
      return platformByService().then((records) => {
        for(let data of records) {
        let status  = null
        let length = data.range[1].length
        if(length > 1)
          if(data.range[1][length - 1] > data.range[1][length - 2])
            status = '#4CAF50'
          else 
            status = '#F44336' 
          else
            status = '#4CAF50'
          
          vm.platformByService.push ({
            percent   : data.percentage +'%',
            barData   : data.range,
            total     : data.total,
            current   : data.current,
            platformType : data.type,
            barOptions: { legend: { display: true} },
            barDatasetOverride: [
            {
              backgroundColor: '#2196F3',
              borderColor    : 'transparent',
              borderWidth    : '5'
            },
            {
              backgroundColor: status,
              borderColor: 'transparent',
              borderWidth: '5'
            }
            ]
          })
        }
        console.log(vm.platformByService)
        return vm.platformByService
        }).catch(function(error){
          logger.error(error.data.msg, error.status)
        }).finally(() => {
          $timeout(() => {
            spinnerService.hide('im-total-stats-spinner')
            spinnerService.hide('sms-total-stats-spinner')
            spinnerService.hide('email-total-stats-spinner')
            spinnerService.hide('mode-total-stats-spinner')
          }, 1000)
        })
}

function lgraphAvgStats() {
  return lineGraph().then(data => {
    let status  = null
    let length = data.range[1].length

    if(length > 1)
      if(data.range[1][length - 1] > data.range[1][length - 2])
        status = '#4CAF50'
      else 
        status = '#F44336' 
      else
        status = '#4CAF50'

      return vm.linegraphDataset = { 
        lgraphPercent        : data.percentage +'%',
        lgraphData           : data.range,
        lgraphTotal          : data.lgraphtotal,
        lgraphCurrent        : data.lgraphcurrent,
        lgraphOptions        : { legend: { display: true} },
        lgraphDatasetOverride: [
        {
          backgroundColor          : '#2196F3',
          borderColor              : '#2196F3',
          fill                     : false,
          borderCapStyle           : 'round',
          borderWidth              : '4',
          pointBorderColor         : '#242633',
          pointBackgroundColor     : "#2196F3",
          pointRadius              : '6',
          pointHoverRadius         : '4',
          pointBorderWidth         : '4',
          pointHoverBackgroundColor: '#2196F3'
        },
        {
          backgroundColor          : status,
          borderColor              : status,
          fill                     : false,
          borderCapStyle           : 'round',
          borderWidth              : '4',
          pointBorderColor         : '#242633',
          pointBackgroundColor     : status,
          pointRadius              : '6',
          pointHoverRadius         : '4',
          pointBorderWidth         : '4',
          pointHoverBackgroundColor: status,
          pointHoverBorderColor    : status
        }
        ] 
      }
    }).catch(function(error){
      logger.error(error.data.msg, error.status)
    }).finally(() => {
      spinnerService.hide('line-chart-spinner')
    })
  }

  //   function imStats() {
  //     return imTotalStats().then(function(data) {
  //       let status  = null
  //       let length = data.range[1].length

  //         if(length > 1)
  //           if(data.range[1][length - 1] > data.range[1][length - 2])
  //             status = '#4CAF50'
  //           else 
  //             status = '#F44336' 
  //         else
  //           status = '#4CAF50'

  //     console.log(status)
  //     return vm.imBarDataset = {
  //       imPercent   : data.percentage +'%',
  //       imBarData   : data.range,
  //       imTotal     : data.imtotal,
  //       imCurrent   : data.imcurrent,
  //       imBarOptions: { legend: { display: true} },
  //       imBarDatasetOverride: [
  //       {
  //         backgroundColor: '#2196F3',
  //         borderColor    : 'transparent',
  //         borderWidth    : '5'
  //       },
  //       {
  //         backgroundColor: status,
  //         borderColor: 'transparent',
  //         borderWidth: '5'
  //       }
  //       ]
  //     }
  //   }).catch(function(error){
  //     logger.error(error.data.msg, error.status)
  //   }).finally(() => {
  //     spinnerService.hide('im-total-stats-spinner')
  //   })
  // }

  // function smsStats() {
  //   return smsTotalStats().then(data => {
  //     return vm.smsBarDataset = {
  //       smsPercent   : data.percentage +'%',
  //       smsBarData   : data.range,
  //       smsTotal     : data.smstotal,
  //       smsCurrent   : data.smscurrent,
  //       smsBarOptions: { legend: { display: true} },
  //       smsBarDatasetOverride: [
  //       {
  //         backgroundColor: '#2196F3',
  //         borderColor    : 'transparent',
  //         borderWidth    : '5'
  //       },
  //       {
  //         backgroundColor: '#4CAF50',
  //         borderColor: 'transparent',
  //         borderWidth: '5'
  //       }
  //       ]
  //     }
  //   }).catch(function(error){
  //     logger.error(error.data.msg, error.status)
  //   }).finally(() => {
  //     spinnerService.hide('sms-total-stats-spinner')
  //   })
  // }

  // function emailStats() {
  //   return emailTotalStats().then(data => {
  //     return vm.emailBarDataset = {
  //       emailPercent   : data.percentage +'%',
  //       emailBarData   : data.range,
  //       emailTotal     : data.emailtotal,
  //       emailCurrent   : data.emailcurrent,
  //       emailBarOptions: { legend: { display: true} },
  //       emailBarDatasetOverride: [
  //       {
  //         backgroundColor: '#2196F3',
  //         borderColor    : 'transparent',
  //         borderWidth    : '5'
  //       },
  //       {
  //         backgroundColor: '#4CAF50',
  //         borderColor: 'transparent',
  //         borderWidth: '5'
  //       }
  //       ]
  //     }
  //   }).catch(function(error){
  //     logger.error(error.data.msg, error.status)
  //   }).finally(() => {
  //     spinnerService.hide('email-total-stats-spinner')
  //   })
  // }

  // function modeStats() {
  //   return modeTotalStats().then(data => {
  //     return vm.modeBarDataset = {
  //       modePercent   : data.percentage +'%',
  //       modeBarData   : data.range,
  //       modeTotal     : data.modetotal,
  //       modeCurrent   : data.modecurrent,
  //       modeBarOptions: { legend: { display: true} },
  //       modeBarDatasetOverride: [
  //       {
  //         backgroundColor: '#2196F3',
  //         borderColor    : 'transparent',
  //         borderWidth    : '5'
  //       },
  //       {
  //         backgroundColor: '#4CAF50',
  //         borderColor: 'transparent',
  //         borderWidth: '5'
  //       }
  //       ]
  //     }
  //   }).catch(function(error){
  //     logger.error(error.data.msg, error.status)
  //   }).finally(() => {
  //     spinnerService.hide('mode-total-stats-spinner')
  //   })
  // }

  function freelancerActivityStats() {
    return freelancerStats().then(data => {
      return vm.freelancerPieDataset = {
        freelancerPieData : data.range,
        freelancerPieColor: ["#4CAF50", "#F44336", "#f2f2f2"],
        freelancerPieOptions: {
          legend: { display: true },
          elements: { 
            arc: { borderWidth: 0 } 
          } 
        }
      }
    }).catch(function(error){
      logger.error(error.data.msg, error.status)
    }).finally(() => {
      spinnerService.hide('fl-activity-stats-spinner')
    })
  }

  function fulltimerActivityStats() {
    fulltimerStats().then(data => {
      return vm.fulltimerPieDataset = {
        fulltimerPieData   : data.range,
        fulltimerPieColor  : ["#4CAF50", "#F44336", "#f2f2f2"],
        fulltimerPieOptions: {
          legend: { display: true },
          elements: { 
            arc: { borderWidth: 0 } 
          } 
        }
      }
    }).catch(function(error){
      logger.error(error.data.msg, error.status)
    }).finally(() => {
      spinnerService.hide('ft-activity-stats-spinner')
    })
  }

  function freelancerTopStats() {
    topFreelancerStats().then(data => {
      vm.topFreelancerStats = data.toplists
      return vm.topFreelancerStats
    }).catch(function(error){
      logger.error(error.data.msg, error.status)
    }).finally(() => {
      spinnerService.hide('top-freelancer-stats-spinner')
    })
  }

  function fulltimerTopStats() {
    topFulltimerStats().then(data => {
      vm.topFulltimerStats = data.toplists
      return vm.topFulltimerStats
    }).catch(function(error){
      logger.error(error.data.msg, error.status)
    }).finally(() => {
      spinnerService.hide('top-fulltimer-stats-spinner')
    })
  }

  function teamTopStats() {
    topTeamStats().then(data => {
      vm.topTeamStats = data.toplists
      return vm.topTeamStats
    }).catch(function(error){
      logger.error(error.data.msg, error.status)
    }).finally(() => {
      spinnerService.hide('top-team-stats-spinner')
    })
  }

  function platformTotalStats() {
    platformStats().then(data => {
      let status  = null
      for(let platforms of data.platformStats) {
        let length = platforms.range[1].length
        
        if(length > 1)
          if(platforms.range[1][length - 1] > platforms.range[1][length - 2])
            status = '#4CAF50'
          else 
            status = '#F44336' 
          else
            status = '#4CAF50'

          platforms['platformLineDatasetOverride'] = [
          {
            backgroundColor          : '#2196F3',
            borderColor              : '#2196F3',
            fill                     : false,
            borderCapStyle           : 'round',
            borderWidth              : '4',
            pointBorderColor         : '#242633',
            pointBackgroundColor     : "#2196F3",
            pointRadius              : '6',
            pointHoverRadius         : '4',
            pointBorderWidth         : '4',
            pointHoverBackgroundColor: '#2196F3'
          },
          {
            backgroundColor          : status,
            borderColor              : status,
            fill                     : false,
            borderCapStyle           : 'round',
            borderWidth              : '4',
            pointBorderColor         : '#242633',
            pointBackgroundColor     : status,
            pointRadius              : '6',
            pointHoverRadius         : '4',
            pointBorderWidth         : '4',
            pointHoverBackgroundColor: status,
            pointHoverBorderColor    : status
          }
          ] 
        }
        return vm.platformStats = data.platformStats
      }).catch(function(error){
        logger.error(error.data.msg, error.status)
      }).finally(() => {
        spinnerService.hide('platforms-stats-spinner')
      })
    }

  }
})()