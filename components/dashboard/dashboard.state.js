(() => {
  'use strict'

  angular.module('prvstrapApp')
  .config(config)

  config.$inject = ["stateHelperProvider", "$urlRouterProvider"]

  function config(stateHelperProvider, $urlRouterProvider) {
    stateHelperProvider
    .setNestedState({
      abstract: true,
      name        : 'main.dashboard',
      url         : '/home',
      data        : { pageTitle: 'Home' },
      access      : { requiredAuthentication: true },
      views       : {
        '': {
          templateUrl : 'components/dashboard/views/dashboard.view.html',
          controller  : "dashboardController",
          controllerAs: 'vm',
        },
        'sub-sidenav@main.dashboard': { 
          templateUrl : 'components/dashboard/views/sub-sidenav.view.html',
        }
      },
      children    : [{
        name    : 'home',
        url     : '',
        data: { subTitle : 'Dashboard'},
        access  : { requiredAuthentication: true },
        views: {
          '': {
            templateUrl : 'components/dashboard/views/main-panel.view.html',
            controller  : 'dashboardHomeController',
            controllerAs: 'vm',
            resolve     : {
              lineGraph         : lineGraph,
              imTotalStats      : imTotalStats,
              smsTotalStats     : smsTotalStats,
              emailTotalStats   : emailTotalStats,
              modeTotalStats    : modeTotalStats,
              freelancerStats   : freelancerStats,
              fulltimerStats    : fulltimerStats,
              topFreelancerStats: topFreelancerStats,
              topFulltimerStats : topFulltimerStats,
              topTeamStats      : topTeamStats,
              platformStats     : platformStats,
              platformByService : platformByService
            }
          },
          'panel@main.dashboard.home': { 
            templateUrl: 'components/dashboard/modules/home/views/home.view.html',
          }
        }
      },
      { 
        name    : 'recent',
        url     : '',
        data: { subTitle : 'Recent Activity'},
        access  : { requiredAuthentication: true },
        views: {
          '': {
            templateUrl: 'components/dashboard/views/main-panel.view.html',
            controller: 'recentController',
            controllerAs: 'vm',
            // resolve    : {
            //   recentSubTitle : recentSubTitle
            // }
          },
          'panel@main.dashboard.recent': { 
            templateUrl: 'components/dashboard/modules/recent/views/recent.view.html',
          }
        }
      }]
    })
    
    $urlRouterProvider.otherwise('/404')
  }

  lineGraph.$injector = ['dashboardHomeService']
  function lineGraph (dashboardHomeService) {
    return dashboardHomeService.lineGraph
  }

  imTotalStats.$injector = ['dashboardHomeService']
  function imTotalStats (dashboardHomeService) {
    return dashboardHomeService.imTotalStats
  }

  smsTotalStats.$injector = ['dashboardHomeService']
  function smsTotalStats (dashboardHomeService) {
    return dashboardHomeService.smsTotalStats
  }

  emailTotalStats.$injector = ['dashboardHomeService']
  function emailTotalStats (dashboardHomeService) {
    return dashboardHomeService.emailTotalStats
  }

  modeTotalStats.$injector = ['dashboardHomeService']
  function modeTotalStats (dashboardHomeService) {
    return dashboardHomeService.modeTotalStats
  }

  freelancerStats.$injector = ['dashboardHomeService']
  function freelancerStats (dashboardHomeService) {
    return dashboardHomeService.freelancerActivityStats
  }

  fulltimerStats.$injector = ['dashboardHomeService']
  function fulltimerStats (dashboardHomeService) {
    return dashboardHomeService.fulltimerActivityStats
  }

  topFreelancerStats.$injector = ['dashboardHomeService']
  function topFreelancerStats (dashboardHomeService) {
    return dashboardHomeService.topFreelancerStats
  }

  topFulltimerStats.$injector = ['dashboardHomeService']
  function topFulltimerStats (dashboardHomeService) {
    return dashboardHomeService.topFulltimerStats
  }

  topTeamStats.$injector = ['dashboardHomeService']
  function topTeamStats (dashboardHomeService) {
    return dashboardHomeService.topTeamStats
  }

  platformStats.$injector = ['dashboardHomeService']
  function platformStats (dashboardHomeService) {
    return dashboardHomeService.platformStats
  }

  platformByService.$injector = ['dashboardHomeService']
  function platformByService (dashboardHomeService) {
    return dashboardHomeService.platformByService
  }

})()

