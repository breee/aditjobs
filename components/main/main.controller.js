(() => {
  'use strict'

  angular.module('aditjobsApp')
  .controller('mainController', mainController);

  mainController.$inject = ['$state', '$http', '$window', '$document', 'AuthenticationService', '$injector', '$animate']

  function mainController($state, $http, $window, $document, AuthenticationService, $injector, $animate) {

    var vm = this
    
    vm.isAuthenticated = AuthenticationService.isAuthenticated

    vm.signup = () => {
      vm.backdropIsOpen = true

      angular.element(document.querySelector('html')).css('overflow-y', 'hidden')
      let backdrop = angular.element(document.querySelector('.signup-panel')).css('display', 'block')
      
      $animate.addClass(backdrop, 'fadeIn').then(() => {
        angular.element(document.querySelector('.step-holder')).addClass('is-open')
        angular.element(document.querySelector('.step-holder .employer-details')).addClass('fadeIn')
      })
    }

    vm.showSubscriptions = () => {
      angular.element(document.querySelector('.step-holder')).removeClass('is-open')
      angular.element(document.querySelector('.step-holder .subscription-list')).removeClass('fadeOut')
      angular.element(document.querySelector('.step-holder')).addClass('is-open-stretch')
      angular.element(document.querySelector('.step-holder .choose-subscription')).addClass('zoomOut')
      angular.element(document.querySelector('.step-holder .proceed-billing')).addClass('zoomIn')
      angular.element(document.querySelector('.step-holder .subscription-list')).addClass('fadeIn')
    }

    $document.on( 'keydown', function (e) {
      if(vm.backdropIsOpen) {
        if (e.keyCode === 27) {
          vm.backdropIsOpen = !vm.backdropIsOpen
        }
      }
    })

  }
})()