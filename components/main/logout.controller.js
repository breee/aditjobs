(() => {
  'use strict'

  angular.module('aditjobsApp')
  .controller('logoutController', logoutController);

  logoutController.$inject = ['$q', '$state', '$http', '$window', 'AuthenticationService', '$injector', 'apiLoginService']

  function logoutController($q, $state, $http, $window, AuthenticationService, $injector, apiLoginService) {

    var vm = this

    activate()

    function activate() {
      var promises = [logout()]
      
      return $q.all(promises).then(function() {
        console.log('Logging out...')
      })
    }

      function logout() {
        // $injector.get('$state').transitionTo('logout')
        return apiLoginService.signOut()
        .then(function () {
          if (AuthenticationService.isAuthenticated) {
            AuthenticationService.isAuthenticated = false
            delete $window.sessionStorage.token
            $state.go('main.home')
          }
        }).finally(function(){ 
          console.log(test)
        })
      }
    
  }
})()