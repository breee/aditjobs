(() => {
	'use strict'

	angular.module('aditjobsApp')
	.config(config)

	config.$inject = ["stateHelperProvider", "$urlRouterProvider"]

	function config(stateHelperProvider, $urlRouterProvider) {
		stateHelperProvider
		.setNestedState({
			abstract: true,
			name        : 'main',
			url         : '',
			views       : {
				'': {
					templateUrl : 'components/main/views/main.view.html',
					controller  : "mainController",
					controllerAs: 'vm',
				},
				'nav-toolbar@main': { 
					templateUrl : 'components/main/views/nav-toolbar.view.html'
				},
				'footer@main': { 
					templateUrl : 'components/main/views/footer.view.html'
				},
				'signup-panel@main': { 
					templateUrl : 'components/main/views/signup-panel.view.html'
				}
			}
		})

		$urlRouterProvider.otherwise('/404')
	}

})()