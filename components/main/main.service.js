(() => {
  'use strict'

  angular
  .module('aditjobsApp')
  .factory('navToolbarService', navToolbarService)

  navToolbarService.$inject = ['$http', '$q', 'CONFIG']

  function navToolbarService($http, $q, CONFIG) {


    return {
      userInfo : userInfo
    }

    function userInfo() {
      let deffered = $q.defer()
      $http({
        method: 'GET',
        url   : `${CONFIG.api_base_url}/dashboard/home`,
        headers : { 'Content-Type' : 'application/json'},
        cache : false
      })
      .then(fetchTokenComplete) 
      .catch(fetchTokenFailed)
      return deffered.promise

      function fetchTokenComplete(response) {
        if (response != undefined && typeof response == "object") {
          deffered.resolve(response.data)
        } else {
          // logger.error('Invalid Server request')
        }
      }

      function fetchTokenFailed(error) {
        deffered.reject(error)
      }
    }

  }
})()