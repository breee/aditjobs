(() => {
  'use strict'

  angular.module('aditjobsApp')
  .controller('homeController', homeController);

  homeController.$inject = ["$q", "$window", "$state", "testimonial", "partners"]

  function homeController($q, $window, $state, testimonial, partners) {

    var vm = this

    vm.testimonials = []
    vm.partners = []
    
    angular.element(document.querySelector('.tagline-title')).addClass('fadeInUp')
    angular.element(document.querySelector('.tagline')).addClass('fadeInUp')
    angular.element(document.querySelector('.search-bar')).addClass('fadeInUp')
    angular.element(document.querySelector('.masthead-content img')).addClass('floating')

    activate()

    function activate() {
      var promises = [getTestimonials(), getPartners()]
      
      return $q.all(promises).then(function() {
        console.log('Activated Dashboard View')
      })
    }

    function getTestimonials() {
      return testimonial().then((testimonials) => {
        vm.testimonials = testimonials.data
          return vm.testimonials
      }).catch(function(error){

      }).finally(() => {
        
      })
    }
    
    function getPartners() {
      return partners().then((partners) => {
        vm.partners = partners.data
          return vm.partners
      }).catch(function(error){

      }).finally(() => {
        
      })
    }
    
  }
})()