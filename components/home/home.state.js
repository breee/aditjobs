(() => {
  'use strict'

  angular.module('aditjobsApp')
  .config(config)

  config.$inject = ["stateHelperProvider", "$urlRouterProvider"]

  function config(stateHelperProvider, $urlRouterProvider) {
    stateHelperProvider
    .setNestedState({
      name        : 'main.home',
      url         : '/',
      views       : {
        '': {
          templateUrl : 'components/home/views/home.view.html',
          controller  : "homeController",
          controllerAs: 'vm',
          resolve: {
            testimonial: testimonial,
            partners: partners,
          }
        },
        'testimonial@main.home': { 
          templateUrl : 'components/home/views/testimonial.view.html',
        },
        'partners@main.home': { 
          templateUrl : 'components/home/views/partners.view.html',
        }
      }
    })
    
    $urlRouterProvider.otherwise('/404')
  }

testimonial.$injector = ['homeService']
  function testimonial (homeService) {
    return homeService.testimonial
}

partners.$injector = ['homeService']
  function partners (homeService) {
    return homeService.partners
}

})()

