(() => {
  'use strict'

  angular
  .module('aditjobsApp')
  .factory('homeService', homeService)

  homeService.$inject = ['$http', '$q', 'CONFIG']

  function homeService($http, $q, CONFIG) {


    return {
      testimonial: testimonial,
      partners: partners
      // signIn : signIn,
      // signOut: signOut
    }

    function testimonial() {
      let deffered = $q.defer()
      $http({
        method: 'GET',
        url   : CONFIG.api_base_url_dev + '/flatdb/testimonial.json',
        headers : { 'Content-Type' : 'application/json'},
        cache : false
      })
      .then(fetchTokenComplete) 
      .catch(fetchTokenFailed)
      return deffered.promise

      function fetchTokenComplete(response) {
        if (response != undefined && typeof response == "object") {
          deffered.resolve(response.data)
        } else {
          console.log('error')
          // logger.error('Invalid Server request')
        }
      }

      function fetchTokenFailed(error) {
        deffered.reject(error)
      }
    }

    function partners() {
      let deffered = $q.defer()
      $http({
        method: 'GET',
        url   : CONFIG.api_base_url_dev + '/flatdb/partners.json',
        headers : { 'Content-Type' : 'application/json'},
        cache : false
      })
      .then(fetchTokenComplete) 
      .catch(fetchTokenFailed)
      return deffered.promise

      function fetchTokenComplete(response) {
        if (response != undefined && typeof response == "object") {
          deffered.resolve(response.data)
        } else {
          console.log('error')
          // logger.error('Invalid Server request')
        }
      }

      function fetchTokenFailed(error) {
        deffered.reject(error)
      }
    }

    // function signIn(username, password) {
    //   let deffered = $q.defer()
    //   $http({
    //     method: 'POST',
    //     url   : CONFIG.api_base_url + '/authenticate/login',
    //     data  : { username: username, password: password},
    //     headers : { 'Content-Type' : 'application/json'},
    //     cache : false
    //   })
    //   .then(fetchTokenComplete) 
    //   .catch(fetchTokenFailed)
    //   return deffered.promise

    //   function fetchTokenComplete(response) {
    //     if (response != undefined && typeof response == "object") {
    //       deffered.resolve(response.data)
    //     } else {
    //       logger.error('Invalid Server request')
    //     }
    //   }

    //   function fetchTokenFailed(error) {
    //     deffered.reject(error)
    //   }
    // }

    // function signOut() {
    //   let deffered = $q.defer()
    //   $http({
    //     method: 'GET',
    //     url   : CONFIG.api_base_url + '/auth/logout',
    //     cache : false
    //   })
    //   .then(logoutComplete) 
    //   .catch(logoutFailed)
    //   return deffered.promise

    //   function logoutComplete(response) {
    //     deffered.resolve(response)
    //   }

    //   function logoutFailed(error) {
    //     deffered.reject(error)
    //   }
    // }
  }
})()