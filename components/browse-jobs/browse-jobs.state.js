(() => {
  'use strict'

  angular.module('aditjobsApp')
  .config(config)

  config.$inject = ["stateHelperProvider", "$urlRouterProvider"]

  function config(stateHelperProvider, $urlRouterProvider) {
    stateHelperProvider
    .setNestedState({
      name        : 'main.browsejobs',
      url         : '/browse-jobs',
      data        : { 
        pageTitle: 'Available jobs for Aditjobs',
        mastheadClass: 'browse-jobs'
      },
      views       : {
        '': {
          templateUrl : 'components/browse-jobs/views/browse-jobs.view.html',
          controller  : "browseJobsController",
          controllerAs: 'vm',
          resolve: {
            jobFilters: jobFilters,
            jobPosts  : jobPosts,
          }
        },
        // 'testimonial@main.home': { 
        //   templateUrl : 'components/home/views/testimonial.view.html',
        // }
      }
    })
    
    $urlRouterProvider.otherwise('/404')
  }

jobFilters.$injector = ['browseJobsService']
  function jobFilters (browseJobsService) {
    return browseJobsService.jobFilters
}

jobPosts.$injector = ['browseJobsService']
  function jobPosts (browseJobsService) {
    return browseJobsService.jobPosts
}

})()

