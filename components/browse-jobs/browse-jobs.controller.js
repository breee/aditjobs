(() => {
  'use strict'

  angular.module('aditjobsApp')
  .controller('browseJobsController', browseJobsController);

  browseJobsController.$inject = ["$q", "$window", "$state", "jobFilters", "jobPosts", "$animate"]

  function browseJobsController($q, $window, $state, jobFilters, jobPosts, $animate) {

    var vm = this

    vm.jobFilters                  = []
    vm.filterModel                 = []
    vm.filterToApi                 = []
    vm.jobPosts                    = []
    vm.filteredList                = []
    vm.chosenJob                   = {}
    vm.filterToApi['job-title']    = []
    vm.filterToApi['job-category'] = []
    vm.filterToApi['job-level']    = []
    vm.filterToApi['job-type']     = []
    vm.filterToApi['job-location'] = []

    vm.itemsPerPage = [{itemCount: "10"}, {itemCount: "20"}, {itemCount: "50"}, {itemCount: "100"}]
    vm.pageSize = { itemCount: "10" }
    
    activate()

    function activate() {
      var promises = [getJobFilters(), getJobPosts()]
      
      return $q.all(promises).then(function() {
        console.log('Activated Dashboard View')
      })
    }

    vm.rowClick = (row, lists) => {
      console.log(row, lists)

      angular.element(document.querySelector('body')).css('overflow', 'hidden')
      angular.element(document.querySelector('.modal-job')).addClass('active')
      angular.element(document.querySelector('.modal-job')).addClass('fadeInRightBig')
      angular.element(document.querySelector('.modal-job')).removeClass('fadeOut')
      angular.element(document.querySelector('.job-form-holder')).removeClass('aside-hidden')
      angular.element(document.querySelector('.modal-job-nav')).removeClass('modal-nav-hidden')

       vm.filteredList = lists

      vm.chosenJob['data'] = row
      let array_index = _.findIndex(vm.filteredList, vm.chosenJob.data)
      
      vm.chosenJob['index'] = array_index
      vm.chosenJob['max'] = vm.filteredList.length-1
    }

    vm.prevJob = (index) => {
      vm.chosenJob.data = vm.filteredList[--index]
      vm.chosenJob.index = index
    }
    
    vm.nextJob = (index) => {
      vm.chosenJob.data = vm.filteredList[++index]
      vm.chosenJob.index = index
    }

    vm.closeModalJob = () => {
      angular.element(document.querySelector('body')).css('overflow', 'auto')
      angular.element(document.querySelector('.modal-job')).removeClass('fadeInRightBig')

      let element = angular.element(document.querySelector('.modal-job'))

      $animate.addClass(element, 'fadeOutRightBig').then(function() {
        angular.element(document.querySelector('.modal-job')).removeClass('active')
        angular.element(document.querySelector('.modal-job')).removeClass('fadeOutRightBig')
        angular.element(document.querySelector('.job-form-holder')).addClass('aside-hidden')
        angular.element(document.querySelector('.modal-job-nav')).addClass('modal-nav-hidden')
      })

    }

    vm.searchMoreJobs = () => {
      angular.element(document.querySelector('.modal')).css('display', 'none')
      vm.closeModalJob()
    }

    vm.closeModal = () => {
      angular.element(document.querySelector('.modal')).css('display', 'none')
    }

    vm.applyJobForm = (form, jobDetails) => {
      console.log(form)
      form.$setPristine()
      form.$setUntouched()
      form.reset();
      //if submission is successfully responded by server show modal
      angular.element(document.querySelector('.modal')).css('display', 'block')
    }

    vm.clearFilter = ()=>{
      vm.filterModel =[]
      vm.filterToApi =[]
    } 

    vm.removeFilter = (filter)=>{
      vm.filterModel.splice(vm.filterModel.indexOf(filter),1)

       switch (filter.title) {
            case 'Post Date':
                alert("Selected Case Number is 1");
                break;
            case 'Job Title':
                vm.filterToApi['job-title'].splice(vm.filterToApi['job-title'].indexOf(filter),1)
                break;
            case 'Job Location':
                    vm.filterToApi['job-category'].splice(vm.filterToApi['job-category'].indexOf(filter),1)
                break;
            case 'Job Category':
                   vm.filterToApi['job-level'].splice(vm.filterToApi['job-level'].indexOf(filter),1)
                break;
            case 'Job Level':
                   vm.filterToApi['job-type'].splice(vm.filterToApi['job-type'].indexOf(filter),1)
                break;
            case 'Job Type':
                   vm.filterToApi['job-location'].splice(vm.filterToApi['job-location'].indexOf(filter),1)
                break;
        }
    }

    function getJobPosts() {
      return jobPosts().then((jobPosts) => {
        vm.jobPosts = jobPosts

        return vm.jobPosts 
      }).catch(function(error){

      }).finally(() => {
      
      })
    }

    function getJobFilters() {
      return jobFilters().then((filters) => {
        let postDataFilter = _.map(filters, 'post-date')[0],
        TitleFilter        = _.map(filters, 'job-title')[0],
        CategoryFilter     = _.map(filters, 'job-category')[0],
        LevelFilter        = _.map(filters, 'job-level')[0],
        TypeFilter         = _.map(filters, 'job-type')[0],
        LocationFilter     = _.map(filters, 'job-location')[0]

        return vm.jobFilters = {
          jobPostDateFilter: postDataFilter,
          jobTitleFilter   : TitleFilter,
          jobCategoryFilter: CategoryFilter,
          jobLevelFilter   : LevelFilter,
          jobTypeFilter    : TypeFilter,
          jobLocationFilter: LocationFilter
        }
      }).catch(function(error){

      }).finally(() => {
      
      })
    }

    vm.postDateSettings = {
      showCheckAll   : false,
      showUncheckAll : false,
      dynamicTitle   : false, 
      closeOnSelect  : true,
      closeOnDeselect: true,
      styleActive    : true,
      selectionLimit : 1
    }

    vm.onPostDateSelect =  {
      onItemSelect: (prop) => {
        vm.filterToApi['post-date'] = prop.label
        console.log(vm.filterToApi)
      },
      onSelectionChanged: () => {
        console.log('api request', vm.filterModel)
      } 
    }

    vm.jobTitleSettings = {
      dynamicTitle   : false,
      closeOnSelect  : true,
      closeOnDeselect: true,
      styleActive    : true,
      checkBoxes     : true,
      scrollable     : true
    }

    vm.onJobTitleSelect =  {
      onItemSelect: (prop) => {
        vm.filterToApi['job-title'].push(prop.label)
        console.log(vm.filterToApi)
      },
      onItemDeselect: (prop) => {
        console.log(prop)
      },
      onDeselectAll: (prop) => {

      },
      onSelectionChanged: () => {
        console.log('api request', vm.filterModel)
      } 
    }

    vm.jobCategorySettings = {
      dynamicTitle   : false,
      closeOnSelect  : true,
      closeOnDeselect: true,
      styleActive    : true,
      checkBoxes     : true,
      scrollable     : true
    }

    vm.onJobCategorySelect =  {
      onItemSelect: (prop) => {
        vm.filterToApi['job-category'].push(prop.label)
        console.log(vm.filterToApi)
      },
      onItemDeselect: (prop) => {
        console.log(prop)
      },
      onSelectionChanged: () => {
        console.log('api request', vm.filterModel)
      } 
    }

    vm.jobLevelSettings = {
      dynamicTitle   : false,
      closeOnSelect  : true,
      closeOnDeselect: true,
      styleActive    : true,
      checkBoxes     : true
    }

    vm.onJobLevelSelect =  {
      onItemSelect: (prop) => {
        vm.filterToApi['job-level'].push(prop.label)
        console.log(vm.filterToApi)
      },
      onItemDeselect: (prop) => {
        console.log(prop)
      },
      onSelectionChanged: () => {
        console.log('api request', vm.filterModel)
      } 
    }

    vm.jobTypeSettings = {
      dynamicTitle   : false,
      closeOnSelect  : true,
      closeOnDeselect: true,
      styleActive    : true,
      checkBoxes     : true
    }

    vm.onJobTypeSelect =  {
      onItemSelect: (prop) => {
        vm.filterToApi['job-type'].push(prop.label)
        console.log(vm.filterToApi)
      },
      onItemDeselect: (prop) => {
        console.log(prop)
      },
      onSelectionChanged: () => {
        console.log('api request', vm.filterModel)
      } 
    }

    vm.jobLocationSettings = {
      dynamicTitle   : false,
      closeOnSelect  : true,
      closeOnDeselect: true,
      styleActive    : true,
      checkBoxes     : true
    }

    vm.onJobLevelSelect =  {
      onItemSelect: (prop) => {
        vm.filterToApi['job-location'].push(prop.label)
        console.log(vm.filterToApi)
      },
      onItemDeselect: (prop) => {
        console.log(prop)
      },
      onSelectionChanged: () => {
        console.log('api request', vm.filterModel)
      } 
    }

    vm.pageChangeHandler = function(num) {
      console.log('going to page ' + num)
    }
    
  }
})()