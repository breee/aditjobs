angular.module('aditjobsApp')
.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', ($stateProvider, $urlRouterProvider, $locationProvider) => {
  // any unknown URLS go to 404
  $urlRouterProvider.otherwise('/404')
  // no route goes to index
  // $urlRouterProvider.when('', '/')
  // $urlRouterProvider.when('/dashboard', '/dashboard/home')
  // $urlRouterProvider.when('/dashboard/', '/dashboard/home')
  
  // use a state provider for routing
  $stateProvider
  .state('404', {
    url        : '/404',
    templateUrl: 'shared/404.html'
  })
  .state('logout', {
    url   : '/logout',
    controller  : "logoutController",
    controllerAs: 'vm',
    access: { requiredAuthentication: true }
  })
  $locationProvider.html5Mode(true);
}]) 
.run(function($rootScope,$stateParams, $state){
  $rootScope.state = $state;
  $rootScope.params = $stateParams;
})
.run(function($rootScope){

  $rootScope.$on('$stateChangeStart',
   function(event, toState  , toParams
     , fromState, fromParams) 
   {
    console.log(toState.data)
  })
})